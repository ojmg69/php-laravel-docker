FROM richarvey/nginx-php-fpm:1.9.1

COPY . .

# Image config
ENV SKIP_COMPOSER 1
ENV WEBROOT /var/www/html/public
ENV PHP_ERRORS_STDERR 1
ENV RUN_SCRIPTS 1
ENV REAL_IP_HEADER 1

# Laravel config
ENV APP_ENV production
ENV APP_DEBUG false
ENV LOG_CHANNEL stderr

# Allow composer to run as root
ENV COMPOSER_ALLOW_SUPERUSER 1

ENV DATABASE_URL postgres://admin:akBLNKyX15CXKrOHcGlXzuD44vTRES9k@dpg-cmk9927109ks739k58jg-a/dbsrf
ENV DB_CONNECTION pgsql
ENV APP_KEY base64:ZZG2CfkQLjSQEVDjFhJ8h3cmXnF34az63M8+fqB5DWs=

CMD ["/start.sh"]
